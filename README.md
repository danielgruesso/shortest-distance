# Shortest Path Finder #

This program finds the shortest path between two cities in a given
road network.

## Run Program ##

To run the code in this folder, issue the following commands in a Linux
terminal:

```bash
$ make
$ ./main < input_data_file_name
```

### Input File Format ###

Input data file should be in the following format:

```
t
n
city_1
city_2
...
city_n
d_11 d_12 ... d_1n
d_21 d_22 ... d_2n
...
d_n1 d_n2 ... d_nn
```

where `t` is the the number of test cases; `n` is the number of
cities; then comes the name of all `n` cities: `city_1, ..., city_n`,
one at each line; and then the matrix that represents the network of
roads between cities and the length of each road.  A positive `d_ij`
represents the length of a road between `city_i` and `city_j`.  A zero
`d_ij` implies there is no direct road from `city_i` to `city_j`.  By
convention we set `d_ii = 0` for all `i = 1, ..., n`.  Note that the
road matrix is symmetric, that is, `d_ij = d_ji`.

### Example ###

So for example, the following input file

```
1
5
Cincinnati
Cleveland
Athens
Pittsburgh
Columbus
0 1 2 4 0
1 0 5 6 7
2 5 0 8 3
4 6 8 0 9
0 7 3 9 0
```

contains 1 test case which consists of 5 cities with the matrix that
represents the road network between the ciites.

The program will then output a sequence of cities that represents the
shortest path from `city_1` to `city_n` along with the total length of the
shortest path.

In our particular example the output would be

```
Cincinnati Athens Columbus 5
```

Which means the shortest path from Cincinnati to Columbus goes through
Athens with a total length of 5.