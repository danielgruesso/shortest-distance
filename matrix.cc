/* ===================================================================
 * Matrix:  class of Square Matrix of n * n dimensions
 * Pooya Taherkhani,  pt375611 @ ohio . edu
 * =================================================================== */

#define NDEBUG			// turn off all assert statements
#include <algorithm>		// for 'copy' which is a 'memcpy' wrapper
#include <iostream>
#include <cassert>		// assert
#include "matrix.h"
using namespace std;

Matrix::Matrix( unsigned n_rows, unsigned m_cols)
  : rows (n_rows)
  , cols (m_cols)
{
  assert( (n_rows != 0 || m_cols != 0) && "Matrix constructor has 0 size");
  int_ptr = new unsigned[ rows * cols];
}

/* ===================================================================
 * lvalue () operator
 * called whenever the object is used as an lvalue
 * return by reference allows for modifying the object
 * =================================================================== */
unsigned& Matrix::operator() (unsigned row, unsigned col)
{
  assert( row < rows && col < cols && "Out of bound array index");
  return int_ptr[ cols*row + col];
}

/* ===================================================================
 * rvalue () operator
 * called whenever the object is used as an rvalue
 * rvalue object is not to be modified,  hence const
 * =================================================================== */
unsigned Matrix::operator() (unsigned row, unsigned col) const
{
  assert( row < rows && col < cols && "Out of bound array index");
  return int_ptr[ cols*row + col];
}

/* ===================================================================
 * The friend version of the function below is written in the comments above
 * I prefer to use the non-friend version below
 * =================================================================== */
bool Matrix::operator ==(const Matrix& other)
{
  bool same_dim = true,
       same_contents = true;
  if (rows != other.rows || cols != other.cols)
    same_dim = false;
  for (unsigned i = 0; i < rows * cols; ++i)
    if (int_ptr[i] != other.int_ptr[i])
      same_contents = false;

  return (same_dim && same_contents);
}

Matrix::~Matrix()
{
  delete [] int_ptr;
}

Matrix::Matrix( const Matrix& other)
{
  rows = other.rows;
  cols = other.cols;
  int_ptr = new unsigned[ rows * cols];

  copy( other.int_ptr, other.int_ptr + rows * cols, int_ptr);
}

/* ===================================================================
 * return Matrix object by reference to allow for chaining the assignment operator
 * return by reference saves a bit of memory space by fewer calls of copy constructor
 * =================================================================== */
Matrix& Matrix::operator =(const Matrix& other)
{
  if (this == &other) return *this;	// self assignment

  rows = other.rows;
  cols = other.cols;
  int_ptr = new unsigned[ rows * cols];
  copy( other.int_ptr, other.int_ptr + rows * cols, int_ptr);
  return *this;
}
