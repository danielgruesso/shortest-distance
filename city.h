#ifndef CITY_H
#define CITY_H

#include <unordered_set>

struct City {
  int number;		       // city number: based on input sequence
  std::string name,	       // city name
              prev;	       // current predecessor city name in shortest path
  unsigned distance;	       // current best estimate for distance from source
};

/* ===================
 * comparison by name to determine the equality
 * used in unordered_set declaration/definition
 * can't modify the existing object in the set.  hence, const
 * only add/remove objects is allowed
 * =================== */
struct comp_name_equal {
  bool operator() (const City& city_a, const City& city_b) const {
    return city_a.number == city_b.number;
  }
};

/* ===================
 * custom hash function based on city number
 * used in unordered_set declaration/definition
 * =================== */
struct city_hash_number {
  size_t operator() (const City& city_a) const {
    return std::hash< int>()( city_a.number);
  }
};

#endif
