/* ===========================================================
 * Shortest path between cities
 * Using Dijkstra's Algorithm
 * Project 6
 * CS 5610,  Data Structures
 * Pooya Taherkhani,  pt376511 @ ohio . edu
 * Based on CLRS textbook,  Chapter 24
 * =========================================================== */

#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>
#include <limits>
#include "heap.h"
#include "matrix.h"
using namespace std;

typedef City_Heap p_queue;
typedef unordered_set< City, city_hash_number, comp_name_equal> u_set;

const unsigned INFINITY = numeric_limits< unsigned>::max();
const string NIL = "";		// predecessor of source

/* ===========================================================
 * Dijkstra:  Compute the distance between two cities: source and destination
 * Input:     weights:      (symmetric) matrix of distances between all cities
 * Output:    cities_vec:   vector of all cities including source and destination
 * =========================================================== */
void dijkstra( int n, vector< City>& cities_vec, const Matrix& weights);

void load_queue( int n, p_queue& cities_queue, const vector< City>& cities_vec);
void print_vec_backward( const vector< City>& cities);
void relax( const City& closest, const Matrix& weights, int i,
	    vector< City>& cities_vec, p_queue& cities_q);

int main()
{
  // read in input and perform Dijkstra
  int test_count,		// number of test cases
      n;			// number of cities
  unsigned weight;		// an edge weight
  City city;
  vector< City> cities;		// a VECTOR of strings

  cin >> test_count;
  for (int i = 0; i < test_count; ++i) { // for each test case
    cin >> n;
    cities.clear();
    for (int j = 0; j < n; ++j) { // for each city
      city.number = j;
      cin >> city.name;

      // initialize_single_source
      city.distance = INFINITY;
      city.prev = NIL;

      cities.push_back( city);
    }
    cities[ 0].distance = 0;	// initialize_single_source ends here
    // cities[ 0]: source,  cities[ n-1]: destination
    
    Matrix weights( n, n);	// square matrix of weights (distances)

    // load distance matrix
    for (int p = 0; p < n; ++p) { // w = [w_pq]
      for (int q = 0; q < n; ++q) {
	cin >> weight;
	weights( p, q) = weight;
      }
    }

    dijkstra( n, cities, weights);

    vector< City> path;
    vector< City>::iterator itr;
    city  = cities[ n-1];

    do {
      path.push_back( city);
      for (itr = cities.begin(); itr != cities.end(); ++itr) {
	if (city.prev == itr->name) {
	  city = *itr;
	  break;
	}
      }
    } while (city.prev != "");

    cout << cities[ 0].name << ' ';
    print_vec_backward( path);
    cout << cities[ n-1].distance << endl;
  }

  return 0;
}

/* ===========================================================
 * Based on CLRS textbook,  Chapter 24
 * =========================================================== */
void dijkstra( int n, vector< City>& cities_vec, const Matrix& weights)
{
  u_set solved_set;
  p_queue cities_queue;	   // priority queue of cities based on current distance

  // initialize_single_source already done
  
  // put all vertices into a priority_queue
  load_queue(n, cities_queue, cities_vec);
  
  while (! cities_queue.empty()) {

    // get closest city to source and remove it from the queue
    City nearest = cities_queue.top_city();
    cities_queue.pop_city();

    solved_set.insert( nearest); // add nearest to solved_set

    for (int i = 0; i < n; ++i) {

      // for adjacent cities to nearest which are not in the solved_set,
      // relax edges connecting nearest to adjacent
      if (i != nearest.number && weights( nearest.number, i) != 0 &&
	  solved_set.find( cities_vec[ i]) == solved_set.end())

	relax(nearest, weights, i, cities_vec, cities_queue);

    }
  }

}

void relax( const City& closest, const Matrix& weights, int i,
	    vector< City>& cities_vec, p_queue& cities_q)
{
  City adjacent = cities_vec[ i];
  if (adjacent.distance > closest.distance + weights( closest.number, i)) {
    adjacent.distance = closest.distance + weights( closest.number, i);
    adjacent.prev = closest.name;

    cities_vec[ i] = adjacent;

    cities_q.update_city( adjacent);
  }
}

void load_queue(int n, p_queue& cities_q, const vector< City>& cities_vec)
{
  for (int i = 0; i < n; ++i) {
    cities_q.add_city( cities_vec[ i]);
  }
}

void print_vec_backward( const vector< City>& cities) {
    // print City vector backwards
    // vector passed as a reference requires const iterator
    vector< City>::const_reverse_iterator itr = cities.rbegin();
    for (; itr != cities.rend(); ++itr) {
      cout << itr->name << ' ';
    }
}
