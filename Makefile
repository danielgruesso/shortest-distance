# Makefile for project 6, CS 5610
# Dijkstra's Algorithm
# Pooya Taherkhani

CC = g++
DEBUG =
# DEBUG = -g  # To debug using gdb, uncomment this line and comment out -O2 optimization flag
FLAGS = -Wall -Wextra -Wpedantic -std=c++11 -Werror -O2
CFLAGS = $(FLAGS) -c $(DEBUG)
LFLAGS = $(FLAGS) $(DEBUG)
EXEC = main
OBJS = $(EXEC).o heap.o matrix.o
HDRS = heap.h matrix.h city.h

$(EXEC): $(OBJS) Makefile
	$(CC) $(LFLAGS) $(OBJS) -o $@

%.o: %.cc $(HDRS) Makefile
	$(CC) $(CFLAGS) $< -o $@

clean:
	$(RM) *.o *~ $(EXEC)
