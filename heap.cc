//*****************************************************************************
//** Heap Data Structure
//** Used in project 6,  Dijkstra's Algorithm
//** CS 5610
//**
//** Last modified by: Pooya Taherkhani
//**
//** All algorithms are implemented based on CLRS algorithms book, chapter 6, to
//** the extent that the problem requirements and practical constraints of
//** implementation have allowed.
//**
//** Pooya Taherkhani,  pt376511 @ ohio . edu
//** 
//*****************************************************************************

#include <iostream>
#include <limits>
#include "heap.h"
using namespace std;
 
const unsigned INFINITY2 = numeric_limits< unsigned>::max();
const string NIL2 = "";		// predecessor of source

void City_Heap::add_city( const City& a_city) {
  //** Add the given city to the heap based on the distance from source.
  //** call:  update_city

  //** For duplicates print a message

  //** linear search to check if city already exists

  vector<City>::iterator itr;
  for (itr = city_heap.begin(); itr != city_heap.end(); ++itr) {
    if (itr->name == a_city.name) {
      cout << "City already in heap" << endl;
      return;
    }
  }

  //** insert city
  city_heap.push_back( a_city);
  update_city( a_city);		// alternatively: this->update_city( new_city);
}

void City_Heap::remove_city(string name) {
  //** Remove city from the heap.
  //** call:  min_heapify

  //** If the city is not found, print a message

  int heap_size = city_heap.size();

  if (heap_size < 1) {
    cout << "No city in heap" << endl;
    return;
  }

  //** indices used, instead of iterators, to be passed to min_heapify
  for (int i = 0; i < heap_size; ++i) {
    if (city_heap[i].name == name) {
      city_heap[i] = city_heap.back();
      city_heap.pop_back();
      min_heapify(i);		// alternatively: this->min_heapify(i);
      return;
    }
  }
  
  cout << "City not found" << endl;
}

void City_Heap::update_city( const City& a_city) {
  //** Update distance from city to source

  //** If the city is not found, print a message

  //** linear search to find city and update distance from city to source
  //** i: vector index to compute node parent 
  int heap_size = city_heap.size();

  for (int i = 0; i < heap_size; ++i) {
    if (city_heap[i].name == a_city.name) {
      //** update distance from city to source
       
      //** restore heap property
      //** while we have't hit root and while parent distance is greater than
      //** node's new distance value:
      //****    assign node its parent--automatic assignment of struct City
      //****    update i to represent node's parent index
      //** when out of loop, assign node new "name" and "distance"
      //** we're done here
      // int score = city_heap[i].distance + distance;
      while (i > 0 && city_heap[(i-1)/2].distance > a_city.distance) {
	city_heap[i] = city_heap[(i-1)/2];
	i = (i - 1) / 2;
      }
      city_heap[i].number = a_city.number;
      city_heap[i].name = a_city.name;
      city_heap[i].distance = a_city.distance;
      city_heap[i].prev = a_city.prev;
      return;

    }
  }

  //** city not found
  cout << "City not found" << endl;
}

void City_Heap::order_cities() {
  //** List the cities in order of distance from
  //** greatest to least. Print both the name and distance from source.
  //** call:  top_city, min_heapify
  
  //** Use heapsort
  vector<City> heap_copy(city_heap);
  vector<City>::reverse_iterator itr;
  for (itr = city_heap.rbegin(); itr != city_heap.rend() - 1; ++itr) {
    print_city_distance( top_city()); // or: print_city_distance( this->top_city());
    city_heap[0] = city_heap.back();
    city_heap.pop_back();
    min_heapify(0);	// or: this->min_heapify(0);
  }
  print_city_distance( top_city()); // or: print_city_distance( this->top_city());

  city_heap = heap_copy;
}

City City_Heap::top_city() {
  //** Return the City with shortest distance
  if (! city_heap.empty())
    return city_heap.front();
  else {
    cout << "Empty database!\n";
    return city_heap.front();
  }
}

void City_Heap::pop_city() {
  //** Remove nearest city to source from the heap
  remove_city( top_city().name); // or: this->remove_city( this->top_city().name);
}

bool City_Heap::empty() {

  return city_heap.empty();
}

void City_Heap::min_heapify(int idx) {
  //** restore heap property rooted at 'idx'
  //** assume both children of root are already heaps
  //** call:  itself

  int left = 2*idx + 1,
    right = 2*idx + 2,
    smallest;

  int heap_size = city_heap.size();
  
  if (left < heap_size &&
      city_heap[left].distance < city_heap[idx].distance)
    smallest = left;
  else
    smallest = idx;
  if (right < heap_size &&
      city_heap[right].distance < city_heap[smallest].distance)
    smallest = right;
  if (smallest != idx) {
    City temp = city_heap[idx];
    city_heap[idx] = city_heap[smallest];
    city_heap[smallest] = temp;

    min_heapify(smallest);	// or: this->min_heapify(smallest);
  }
}

void City_Heap::print_city_distance(const City& a_city) {
  //** Print city name and its distance from source
  cout << a_city.name << ": " << a_city.distance << endl;
}
