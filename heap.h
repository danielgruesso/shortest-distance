//*******************************************************
//** Heap
//** Implementation of priority queue abstract data type
//** Used in project 6,  Dijkstra's Algorithm
//**
//** Pooya Taherkhani,  pt376511 @ ohio . edu
//**
//*******************************************************

#ifndef HEAP_H
#define HEAP_H

#include <vector> 
#include <string>
#include "city.h"

class City_Heap {
 private:
  void min_heapify(int idx);
  void print_city_distance(const City& a_city);
  std::vector<City> city_heap; 

 public:
  void add_city( const City& a_city); 
  void remove_city(std::string name); 

  void update_city( const City& a_city); 

  void order_cities(); 
  City top_city(); 
  void pop_city();

  bool empty();
};

#endif
